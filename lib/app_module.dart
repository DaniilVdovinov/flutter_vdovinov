import 'package:first_project/homework_module/home_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {

  @override
  List<ModularRoute> get routes => [
    ModuleRoute("/", module: HomeModule())
  ];
}