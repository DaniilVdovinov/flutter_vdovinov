import 'package:first_project/homework_module/homework3/dto/image_dto.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:image_picker/image_picker.dart';

part 'image_store.g.dart';

class ImageStore = _ImageStore with _$ImageStore;

abstract class _ImageStore with Store {
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();

  @observable
  ObservableList<ImageDto> images = ObservableList.of([
    ImageDto(image: Image.asset("assets/images/image_1.jpg"), name: "Forest")
  ]);

  ImagePicker picker = ImagePicker();

  @action
  void addImage(ImageSource imageSource, Function callback) {
    picker.pickImage(source: imageSource).then((file) => {
          file!.readAsBytes().then((value) {
            images.add(ImageDto(image: Image.memory(value), name: file.name));
            callback.call();
          })
        });
  }

  int getLastIndex() {
    return images.length - 1;
  }
}
