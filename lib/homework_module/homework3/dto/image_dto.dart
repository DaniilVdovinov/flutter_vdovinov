import 'package:flutter/material.dart';

class ImageDto {
  Image image;
  String name;

  ImageDto({required this.image, required this.name});
}
