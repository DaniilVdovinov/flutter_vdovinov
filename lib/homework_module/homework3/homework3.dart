import 'package:first_project/homework_module/homework3/dto/image_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../homework.dart';
import 'image_store.dart';

class Homework3 extends Homework {
  Homework3() : super('HW3');

  @override
  State<StatefulWidget> createState() => Homework3State();
}

class Homework3State extends State<Homework3> {
  var imageStore = Modular.get<ImageStore>();
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Gallery"),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: GestureDetector(
                onTap: () => imageStore.addImage(ImageSource.gallery, () {
                  listKey.currentState!.insertItem(imageStore.getLastIndex());
                }),
                child: const Icon(Icons.add_box),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 50.0),
              child: GestureDetector(
                onTap: () => imageStore.addImage(ImageSource.camera, () {
                  listKey.currentState!.insertItem(imageStore.getLastIndex());
                }),
                child: const Icon(Icons.add_a_photo),
              ),
            ),
          ],
        ),
        body: Observer(builder: (context) {
          return AnimatedList(
              key: listKey,
              scrollDirection: Axis.vertical,
              initialItemCount: imageStore.images.length,
              itemBuilder: (context, index, animation) {
                ImageDto img = imageStore.images[index];
                return Padding(
                  padding: const EdgeInsets.all(16),
                  child: SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(-1, 0),
                      end: const Offset(0, 0),
                    ).animate(CurvedAnimation(
                        parent: animation,
                        curve: Curves.easeIn,
                        reverseCurve: Curves.bounceOut)),
                    child: GestureDetector(
                      onTap: () =>
                          Modular.to.pushNamed("/hw/3/info", arguments: img),
                      child: Hero(
                        tag: img.name,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: Stack(children: [
                            img.image,
                            buildGradient(),
                            buildTitle(img.name)
                          ]),
                        ),
                      ),
                    ),
                  ),
                );
              });
        }));
  }

  Widget buildGradient() {
    return Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.transparent, Colors.black.withOpacity(0.7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: const [0.6, 0.95],
          ),
        ),
      ),
    );
  }

  Widget buildTitle(String name) {
    return Positioned(
      left: 40,
      bottom: 30,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
