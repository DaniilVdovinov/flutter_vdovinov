import 'package:flutter/material.dart';
import 'package:mouse_parallax/mouse_parallax.dart';

import 'dto/image_dto.dart';

class ImageModal extends StatefulWidget {
  ImageDto image;

  ImageModal(this.image);

  @override
  State<StatefulWidget> createState() => ImageModalState();
}

class ImageModalState extends State<ImageModal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.image.name),
      ),
      body: SafeArea(
        child: ParallaxStack(
          layers: [
            ParallaxLayer(
                yRotation: 0.2,
                xRotation: 0.2,
                xOffset: 30,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: widget.image.image.image,
                          colorFilter: ColorFilter.mode(
                              Colors.black.withOpacity(0.5),
                              BlendMode.srcOver))),
                )),
            ParallaxLayer(
              yRotation: 0.3,
              xRotation: 0.3,
              xOffset: 60,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Hero(
                          tag: widget.image.name,
                          child: InteractiveViewer(child: widget.image.image),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.image.name,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              )),
                        )
                      ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
