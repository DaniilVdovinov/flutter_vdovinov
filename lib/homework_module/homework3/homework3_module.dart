import 'package:first_project/homework_module/homework3/image_modal.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'homework3.dart';
import 'image_store.dart';

class Homework3Module extends Module {
  @override
  List<ModularRoute> get routes => [
        ChildRoute("/", child: (context, args) => Homework3()),
        ChildRoute("/info",
            child: (context, args) => ImageModal(args.data),
            transition: TransitionType.rightToLeftWithFade)
      ];

  @override
  List<Bind> get binds => [Bind.singleton((i) => ImageStore())];
}
