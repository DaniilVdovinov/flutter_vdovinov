import 'package:flutter/material.dart';

import '../../homework.dart';

class Homework1 extends Homework {
  Homework1() : super('HW1');

  @override
  State<StatefulWidget> createState() => Homework1State();
}

enum MessageType { income, outcome }

class Message {
  String text;
  DateTime dateTime = DateTime.now();
  String user;
  MessageType type;

  Message(this.text, this.user, this.type);
}

class Homework1State extends State<Homework1> {
  TextEditingController user1TextController = TextEditingController();
  TextEditingController user2TextController = TextEditingController();

  final List<Message> messages = List.empty(growable: true);

  void addMessage1(String text) {
    setState(() {
      messages.add(Message(text, 'User 1', MessageType.income));
    });
    user1TextController.clear();
  }

  void addMessage2(String text) {
    setState(() {
      messages.add(Message(text, 'User 2', MessageType.outcome));
    });
    user2TextController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: messages.map((item) {
                    return Card(
                        child: ListTile(
                            title: Text(
                              item.user + ' ' + item.dateTime.toString(),
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 14),
                                textAlign: item.type == MessageType.income
                                    ? TextAlign.left
                                    : TextAlign.right
                            ),
                            subtitle: Text(item.text,
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                textAlign: item.type == MessageType.income
                                    ? TextAlign.left
                                    : TextAlign.right)));
                  }).toList(),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: user1TextController,
                    ),
                  ),
                  GestureDetector(
                    child: const Icon(Icons.add),
                    onTap: () {
                      addMessage1(user1TextController.text);
                    },
                  ),
                  Expanded(
                    child: TextField(
                      controller: user2TextController,
                    ),
                  ),
                  GestureDetector(
                    child: const Icon(Icons.add),
                    onTap: () {
                      addMessage2(user2TextController.text);
                    },
                  )
                ],
              ),
            ],
          ),
        )),
      ),
    );
  }
}
