import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';

import 'chat_client.dart';
import 'dto/message.dart';

part 'message_store.g.dart';

class MessageStore = _MessageStore with _$MessageStore;

abstract class _MessageStore with Store {
  @observable
  ObservableList<ChatMessage> messages = ObservableList.of([]);

  ChatClient chatClient = ChatClient(Dio());

  @action
  void getNewMessages(Function callback) {
    chatClient.getMessages().then((List<ChatMessage> messages) {
      this.messages = ObservableList.of(messages);
      callback.call();
    }).catchError((e) {
      throw e;
    });
  }

  @action
  void sendMessage(ChatMessage message) {
    chatClient.sendMessage(message).then((ChatMessage message) {
      messages.add(message);
    }).catchError((e) {
      throw e;
    });
  }
}
