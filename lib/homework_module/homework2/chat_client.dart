import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import 'dto/message.dart';

part 'chat_client.g.dart';

@RestApi(baseUrl: "https://itis-chat-app-ex.herokuapp.com/chat")
abstract class ChatClient {
  factory ChatClient(Dio dio, {String baseUrl}) = _ChatClient;

  @GET("")
  Future<List<ChatMessage>> getMessages();

  @POST("")
  Future<ChatMessage> sendMessage(@Body() ChatMessage message);
}