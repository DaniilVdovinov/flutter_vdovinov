import 'package:first_project/homework_module/homework2/message_store.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'homework2.dart';

class Homework2Module extends Module {

  @override
  List<Bind> get binds => [
    Bind.singleton((i) => MessageStore())
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute("/", child: (context, args) => Homework2())
  ];
}