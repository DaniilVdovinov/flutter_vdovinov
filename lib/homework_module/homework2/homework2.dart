import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../homework.dart';
import 'dto/message.dart';
import 'message_store.dart';


class Homework2 extends Homework {
  Homework2() : super('HW2');

  @override
  State<StatefulWidget> createState() => Homework2State();
}

class Homework2State extends State<Homework2> {
  TextEditingController textController = TextEditingController();

  MessageStore messageStore = Modular.get<MessageStore>();

  @override
  void initState() {
    super.initState();
    EasyLoading.show(status: 'Loading messages');
    messageStore.getNewMessages(() {
      EasyLoading.dismiss();
      EasyLoading.showSuccess('Loaded!');
    });
  }

  void addMessage(String text) {
    messageStore.sendMessage(ChatMessage(author: 'Daniil', message: text));
    textController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Expanded(
                child: Observer(builder: (context) {
                  return ListView(
                    children: messageStore.messages.map((item) {
                      return Card(
                          child: ListTile(
                              title: Text(item.author,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 14)),
                              subtitle: Text(item.message,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 16))));
                    }).toList(),
                  );
                }),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: textController,
                    ),
                  ),
                  GestureDetector(
                    child: const Icon(Icons.add),
                    onTap: () {
                      addMessage(textController.text);
                    },
                  )
                ],
              ),
            ],
          ),
        )),
      ),
    );
  }
}
