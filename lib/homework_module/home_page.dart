import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:collection/collection.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> homeworkRoutes = ["/hw/1", "/hw/2", "/hw/3"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(widget.title)),
        body: Center(
            child: ListView(
          children: homeworkRoutes
              .mapIndexed((index, e) => ListTile(
                  title: Text("Homework " + (index + 1).toString()), onTap: () => Modular.to.pushNamed(e)))
              .toList(),
        )));
  }
}
