import 'package:first_project/homework_module/homework2/homework2_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_page.dart';
import 'homework1/homework1.dart';
import 'homework3/homework3_module.dart';

class HomeModule extends Module {

  @override
  List<ModularRoute> get routes => [
    ChildRoute("/", child: (context, args)  => const MyHomePage(title: "Homeworks")),
    ChildRoute("/hw/1", child: (context, args) => Homework1()),
    ModuleRoute("/hw/2", module: Homework2Module()),
    ModuleRoute("/hw/3", module: Homework3Module()),
  ];
}