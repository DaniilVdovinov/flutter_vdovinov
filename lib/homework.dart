import 'package:flutter/material.dart';

abstract class Homework extends StatefulWidget {
  final String title;

  Homework(this.title);
}